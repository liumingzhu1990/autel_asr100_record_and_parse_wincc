﻿
// Autel_AS100__Dlg.h: 头文件
//

#pragma once
#include <queue>
#include "ControlCAN.h"
using namespace std;

struct Point2f {
	float x;
	float y;
};

struct SRRObstacle {
	bool is_cipv;
	UINT8 frame_order1;
	UINT8 frame_order2;
	UINT8 frame_order3;
	UINT8 object_id;
	UINT8 tracking_num;
	UINT8 warning_grade;

	bool is_valid_time;	
	char time_stamp_str[20];
	ULONGLONG system_time;

	struct Point2f ttc;
	struct Point2f dist;
	struct Point2f vel;
	struct Point2f acc;

	UINT8 warning_zone;
	UINT8 motion_state;
	UINT8 malfunction;

	UINT8 move_obj_num;
	UINT8 stop_obj_num;
	UINT8 static_obj_num;
	UINT8 total_obj_num;

	bool is_valid_carSpeed;
	float car_speed;
	float car_acceleration;
	float car_angle_speed;
};

struct Judge_Str {
	volatile bool is_full_9_frame_sw;		// 队列是否满9组数据
	int pop_cnt;					// pop计数
	int object_num;				// 队列中接收的所有数据

	int dispose_times;			// 要处理的数据条数，9的整数倍
	volatile UINT8 array_index;				// 输出打印的数据下标
	UINT8 array_index_multi;				// 计算下标用到倍数

	volatile UINT8 move_obj_num;
	volatile UINT8 static_obj_num;
	volatile UINT8 stop_obj_num;

};

typedef struct _CAN_Show_Info
{
	VCI_CAN_OBJ CanInfo;
	char	TimeStr[20];	// 解析CSV的时间戳
}CAN_Show_Info;

struct VehicleCan {
	uint8_t right_turn;
	uint8_t left_turn;
	uint8_t reversing;
	uint8_t brake_status;
	float car_speed;
	float yaw_rate;
	float x_accel;
};
enum SRRCarZone {
	UNKNOWN_CAR_ZONE = 0, RIGHT_MIDDLE_CAR_ZONE, RIGHT_BACK_CAR_ZONE, RIGHT_FRONT_CAR_ZONE,
};

enum SRRMOtionState {
	UNKNOWN_MOTION_STATE = 0, MOVING_MOTION_STATE, STOPED_MOTION_STATE, STANDING_MOTION_STATE,
};

enum SRRMalfunction {
	SENSOR_OK = 0, SENSOR_FAILURE, SENSOR_PERFORM_DECREASED, SENSOR_COMMUNICATION_FAILURE,
};

/* 波特率枚举 */
enum _BaudRate
{
	BR_10K = 0,
	BR_20K,
	BR_50K,
	BR_125K,
	BR_250K,
	BR_500K,
	BR_800K,
	BR_1000K
};
// CAutelAS100Dlg 对话框
class CAutelAS100Dlg : public CDialogEx
{
// 构造
public:
	CAutelAS100Dlg(CWnd* pParent = nullptr);	// 标准构造函数
	~CAutelAS100Dlg();	//析构函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_AUTEL_AS100___DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

public:
	//CDialogEx cdlgEx;
	//queue<VCI_CAN_OBJ> m_canInfo_queue;		// 存储CAN数据队列（读取后直接缓存）
	queue<CAN_Show_Info> m_canInfo_queue;		// 存储CAN数据队列（读取后直接缓存）
	queue<CAN_Show_Info> m_canSpeed_queue;		// 存储CAN数据队列:0x760车速信息
	struct SRRObstacle m_srr_obstacle_info[3];
	struct VehicleCan vehicleCan_info;

	FILE* pFile_handle;						// 写文件句柄

	// btn采集按钮标志
	bool m_btn_func_sw;
	// 接受线程
	bool m_thread_recv_is_stop;

	// 处理线程
	bool m_thread_process_is_stop;		// true停止，false执行中
	bool m_thread_process_is_working;	// true工作中，false等待中
	//HANDLE m_hThread_process;

	// 获取CSV文件信息
	bool m_btn_anlysis_sw;
	CFile m_csv_cfile_handle;
	long m_csv_file_length;
	bool m_thread_csv_is_stop;

	CEdit m_edit_object_num_handle;
	CEdit m_edit_move_object_num_handle;
	CEdit m_edit_stop_object_num_handle;
	CEdit m_edit_static_object_num_handle;
	CListBox m_lis_show_info_handle;
	CEdit m_edit_vehicle_spd_handle;
	CEdit m_imu_x_acc_handle;
	CEdit m_imu_z_yaw_handle;
	CComboBox m_combox_can_index_handle;
	CButton m_btn_func_sw_handle;
	CMFCEditBrowseCtrl m_editBrowse_handle;
	CButton m_btn_cvs_analysis_handle;

	// listbox显示内容
	CComboBox m_combox_show_listbox_handle;
	bool m_listbox_is_show;

	// 用时间对数据进行过滤
	CComboBox m_combox_begin_hour_handle;
	CComboBox m_combox_begin_min_handle;
	CComboBox m_combox_end_hour_handle;
	CComboBox m_combox_end_min_handle;
	char m_time_begin_filter_str[20];
	char m_time_end_filter_str[20];
	bool m_time_is_filter;
	bool m_time_filter_is_trig;
	
	//// Data-Time Control 查询时间起止显示
	//CDateTimeCtrl m_datatimeControl_begin_handle;
	//CDateTimeCtrl m_datatimeControl_end_handle;

	static UINT ReceiveThread(void *param);
	static UINT ProcessThread(void *param);
	static UINT AnalyCSVFileThread(void *param);
	//bool start_CAN(int devInd, enum _BaudRate BaudRat);
	//void RecvFrame_ErrorFormation(void* errinfo);

	void In_Listbox_Show_SRR_Info(struct SRRObstacle *p_info);
	void List_Show_Info(char* data);

	void Create_File_To_Write_Log_Info();
	void Dlg_Front_Init();
	bool mySetFontSize(int ID, float font_size);
// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	afx_msg void OnBnClickedButton2();
	CComboBox m_combox_begin_second_handle;
	CComboBox m_combox_end_second_handle;
};
