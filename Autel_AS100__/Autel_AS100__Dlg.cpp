﻿
// Autel_AS100__Dlg.cpp: 实现文件
//

#include "stdafx.h"
#include "Autel_AS100__.h"
#include "Autel_AS100__Dlg.h"
//#include "afxdialogex.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

int m_devtype = VCI_USBCAN2;
int m_devind = 0;
int m_CANInd = 0;
bool g_recv_is_stop = false;

/*****************************************
函数说明：CAN本身机制，异常消息提示
函数返回值：
*****************************************/
void RecvFrame_ErrorFormation(void* errinfo)
{
	VCI_ERR_INFO* errorInfo = (VCI_ERR_INFO*)(errinfo);
	CString msg;

	switch (errorInfo->ErrCode)
	{
	case 0x0100:	// 设备已经打开
	{
		printf("设备已经打开");
	}break;
	case 0x0200:	// 打开设备错误
	{
		printf("打开设备错误");
	}break;
	case 0x0400:	// 设备没有打开
	{
		printf("设备没有打开");
	}break;
	case 0x0800:	// 缓冲区溢出
	{
		printf("缓冲区溢出");
	}break;
	case 0x1000:	// 此设备不存在
	{
		printf("此设备不存在");
	}break;
	case 0x2000:	// 装载动态库失败
	{
		printf("装载动态库失败");
	}break;
	case 0x4000:	// 表示为执行命令失败错误
	{
		printf("表示为执行命令失败错误");
	}break;
	case 0x8000:	// 内存不足
	{
		printf("内存不足");
	}break;
	case 0x0001:	// CAN 控制器内部 FIFO 溢出
	{
		printf("CAN 控制器内部 FIFO 溢出");
	}break;
	case 0x0002:	// CAN 控制器错误报警
	{
		printf("CAN 控制器错误报警");
	}break;
	case 0x0004:	// CAN 控制器消极错误
	{
		printf("CAN 控制器消极错误");
	}break;
	case 0x0008:	// CAN 控制器仲裁丢失
	{
		printf("CAN 控制器仲裁丢失");
	}break;
	case 0x0010:	// CAN 控制器总线错误
	{
		printf("CAN 控制器总线错误");
	}break;
	default:break;
	}
}
static UINT ReceiveThread1(void *param)
{
	//CAutelAS100Dlg *pthread = (CAutelAS100Dlg*)param;
	VCI_CAN_OBJ frameinfo[50];
	VCI_ERR_INFO errinfo;
	int len = 0;
	int clac_760_times = 0;
	//long can_cache_len = 0;
	printf("recv thread\n");

	while (!g_recv_is_stop)
	{
		//Sleep(1);
		//can_cache_len = VCI_GetReceiveNum(m_devtype, m_devind, m_CANInd);
		//if (can_cache_len <= 0) {
		//	Sleep(1);
		//	continue;
		//}

		len = VCI_Receive(m_devtype, m_devind, m_CANInd, frameinfo, 50, 200);
		if (len <= 0)
		{
			//注意：如果没有读到数据则必须调用此函数来读取出当前的错误码，
			//千万不能省略这一步（即使你可能不想知道错误码是什么）
			VCI_ReadErrInfo(m_devtype, m_devind, m_CANInd, &errinfo);
			/* 异常报错弹框 */
			//pthread->RecvFrame_ErrorFormation(&errinfo);
		}
		else
		{
			for (int i = 0; i < len; i++)
			{
				if (frameinfo[i].ID >= 0x750 && frameinfo[i].ID <= 0x758) {
					printf("0x%03X\n", frameinfo[i].ID);
				}

			}
		}
	}
	return 1;
}

UINT CAutelAS100Dlg::ReceiveThread(void *param)
{
#define RECV_LEN		50
	CAutelAS100Dlg *pthread = (CAutelAS100Dlg*)param;
	VCI_CAN_OBJ frameinfo[RECV_LEN];
	VCI_ERR_INFO errinfo;
	int len = 0;
	int clac_760_times = 0;
	//long can_cache_len = 0;
	printf("recv thread\n");
	
	//pthread->m_btn_func_sw_handle.SetWindowTextA("采集中");
	while (!pthread->m_thread_recv_is_stop)
	{
		Sleep(1);
		len = VCI_Receive(m_devtype, m_devind, m_CANInd, frameinfo, RECV_LEN, 200);
		if (len <= 0)
		{
			//注意：如果没有读到数据则必须调用此函数来读取出当前的错误码，
			//千万不能省略这一步（即使你可能不想知道错误码是什么）
			VCI_ReadErrInfo(m_devtype, m_devind, m_CANInd, &errinfo);
			/* 异常报错弹框 */
			//RecvFrame_ErrorFormation(&errinfo);
		}
		else
		{
			for (int i = 0; i < len; i++)
			{
				//if (frameinfo[i].ID >= 0x750 && frameinfo[i].ID <= 0x758) {
				//	printf("0x%03X\n", frameinfo[i].ID);
				//}

				//如果是数据帧，获取数据帧里面的数据
				if (frameinfo[i].RemoteFlag == 0 && frameinfo[i].ExternFlag == 0)
				{
					if (frameinfo[i].ID >= 0x750 && frameinfo[i].ID <= 0x758) {
						printf("0x%03X\n", frameinfo[i].ID);
						CAN_Show_Info caninfo;
						caninfo.CanInfo = frameinfo[i];
						pthread->m_canInfo_queue.push(caninfo);
					}
					else if (frameinfo[i].ID == 0x760) {
						////  10次解析一次
						if (clac_760_times > 20) {
							clac_760_times = 0;
							pthread->vehicleCan_info.right_turn = frameinfo[i].Data[0] & 0x01;
							pthread->vehicleCan_info.left_turn = (frameinfo[i].Data[0] >> 1) & 0x01;
							pthread->vehicleCan_info.reversing = (frameinfo[i].Data[0] >> 3) & 0x03;
							pthread->vehicleCan_info.brake_status = (frameinfo[i].Data[0] >> 5) & 0x03;
							pthread->vehicleCan_info.car_speed = ((float)(frameinfo[i].Data[1] | (frameinfo[i].Data[2] << 8)) - 0.5f) / 3.6f / 128.0f;

							if (frameinfo[i].Data[3] == 0xFF && frameinfo[i].Data[4] == 0xFF) pthread->vehicleCan_info.yaw_rate = 0.0f;
							else pthread->vehicleCan_info.yaw_rate = (float)(((frameinfo[i].Data[3] + frameinfo[i].Data[4] * 255) - 0.5f) / 100.0 - 327.67);


							if (frameinfo[i].Data[5] == 0xFF && frameinfo[i].Data[6] == 0xFF) pthread->vehicleCan_info.x_accel = 0.0f;
							else pthread->vehicleCan_info.x_accel = (float)(((frameinfo[i].Data[5] + frameinfo[i].Data[6] * 255) - 0.5f) * 16.0 / 32768.0 - 16.0);
						}
						else clac_760_times++;
					}
				}
			}
		}
	}
	printf("recv thread end\n");
	return 0;
}
//bool CAutelAS100Dlg::start_CAN(int devInd, enum _BaudRate BaudRat)
bool start_CAN()
{
	VCI_INIT_CONFIG init_config;

	init_config.AccCode = 0;			// 验收码
	init_config.AccMask = 0xFFFFFFFF;	// 屏蔽码
	init_config.Filter = 0;				// 滤波方式
	init_config.Mode = 0;				// 工作模式
	init_config.Timing0 = 0x00;	// 定时器0
	init_config.Timing1 = 0x1C;	// 定时器1
	///* 设置波特率 */
	//switch (BaudRat){
	//	case BR_10K:		// 10Kbps
	//	{
	//		init_config.Timing0 = 0x31;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_20K:		// 20Kbps
	//	{
	//		init_config.Timing0 = 0x18;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_50K:		// 50Kbps
	//	{
	//		init_config.Timing0 = 0x09;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_125K:		// 125Kbps
	//	{
	//		init_config.Timing0 = 0x03;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_250K:		// 250Kbps
	//	{
	//		init_config.Timing0 = 0x01;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_500K:		// 500Kbps
	//	{
	//		init_config.Timing0 = 0x00;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_800K:		// 800Kbps
	//	{
	//		init_config.Timing0 = 0x00;	// 定时器0
	//		init_config.Timing1 = 0x16;	// 定时器1
	//	}break;
	//	case BR_1000K:		// 1000Kbps
	//	{
	//		init_config.Timing0 = 0x00;	// 定时器0
	//		init_config.Timing1 = 0x14;	// 定时器1
	//	}break;
	//	default:break;
	//}

	/* 打开USBCAN设备 */
	if (VCI_OpenDevice(m_devtype, m_devind, 5) != STATUS_OK) // 500Kbps
	{
		printf("打开设备失败!\n");
		return false;
	}
	else
	{
		printf("打开USBCAN设备成功\n");
	}



	/* 初始化USBCAN设备 */
	if (VCI_InitCAN(m_devtype, m_devind, m_CANInd, &init_config) != STATUS_OK)	{
		printf("初始化CAN失败!\n");
		VCI_CloseDevice(m_devtype, m_devind);
		return false;
	}else{
		printf("初始化USBCAN设备成功!\n");
	}

	if (VCI_StartCAN(m_devtype, m_devind, m_CANInd) == 1){
		printf("启动成功\n");
	}else{
		printf("启动失败\n");
		return false;
	}

	return true;
}

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CAutelAS100Dlg 对话框



CAutelAS100Dlg::CAutelAS100Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_AUTEL_AS100___DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_devtype = VCI_USBCAN2;
	m_devind = 0;
	m_CANInd=0;
	memset(&vehicleCan_info, 0, sizeof(struct VehicleCan));
	for (int i = 0; i < 3;i++) {
		memset(&m_srr_obstacle_info[i], 0, sizeof(struct SRRObstacle));
	}
	pFile_handle = NULL;
	m_thread_recv_is_stop = false;
	m_thread_process_is_stop = false;
	m_thread_csv_is_stop = false;

	m_btn_func_sw = false;
	m_btn_anlysis_sw = false;
	m_listbox_is_show = false;
	m_time_is_filter = false;
	m_time_filter_is_trig = false;
}

CAutelAS100Dlg::~CAutelAS100Dlg()
{
	m_thread_process_is_stop = true;

	if (pFile_handle != NULL) {
		fclose(pFile_handle);
		pFile_handle = NULL;
	}

	if (m_thread_recv_is_stop ==false) {
		m_thread_recv_is_stop = true;
	}	
	
	if (m_thread_csv_is_stop ==false) {
		m_thread_csv_is_stop = true;
	}
}

void CAutelAS100Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edit_object_num_handle);
	DDX_Control(pDX, IDC_EDIT2, m_edit_move_object_num_handle);
	DDX_Control(pDX, IDC_EDIT3, m_edit_stop_object_num_handle);
	DDX_Control(pDX, IDC_EDIT4, m_edit_static_object_num_handle);
	DDX_Control(pDX, IDC_LIST1, m_lis_show_info_handle);
	DDX_Control(pDX, IDC_EDIT5, m_edit_vehicle_spd_handle);
	DDX_Control(pDX, IDC_EDIT6, m_imu_x_acc_handle);
	DDX_Control(pDX, IDC_EDIT7, m_imu_z_yaw_handle);
	DDX_Control(pDX, IDC_COMBO2, m_combox_can_index_handle);
	DDX_Control(pDX, IDC_BUTTON1, m_btn_func_sw_handle);
	DDX_Control(pDX, IDC_MFCEDITBROWSE1, m_editBrowse_handle);
	DDX_Control(pDX, IDC_BUTTON2, m_btn_cvs_analysis_handle);
	DDX_Control(pDX, IDC_COMBO1, m_combox_show_listbox_handle);
	DDX_Control(pDX, IDC_COMBO3, m_combox_begin_hour_handle);
	DDX_Control(pDX, IDC_COMBO4, m_combox_begin_min_handle);
	DDX_Control(pDX, IDC_COMBO5, m_combox_end_hour_handle);
	DDX_Control(pDX, IDC_COMBO6, m_combox_end_min_handle);
	DDX_Control(pDX, IDC_COMBO7, m_combox_begin_second_handle);
	DDX_Control(pDX, IDC_COMBO8, m_combox_end_second_handle);
}

BEGIN_MESSAGE_MAP(CAutelAS100Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CAutelAS100Dlg::OnBnClickedButton1)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON2, &CAutelAS100Dlg::OnBnClickedButton2)
END_MESSAGE_MAP()

bool CAutelAS100Dlg::mySetFontSize(int ID, float font_size)
{
	LOGFONT   logfont;//最好弄成类成员,全局变量,静态成员  
	CFont* pfont1 = GetDlgItem(ID)->GetFont();
	pfont1->GetLogFont(&logfont);
	logfont.lfHeight = (LONG)(logfont.lfHeight * font_size);   //这里可以修改字体的高比例
	logfont.lfWidth = (LONG)(logfont.lfWidth * font_size);   //这里可以修改字体的宽比例
	static   CFont   font1;
	font1.CreateFontIndirect(&logfont);
	GetDlgItem(ID)->SetFont(&font1);
	font1.Detach();

	return TRUE;
}

void CAutelAS100Dlg::Dlg_Front_Init()
{
#define CONTROLER_SIZE		1.5f
	mySetFontSize(IDC_LIST1, CONTROLER_SIZE-0.2f);		// 展示区

	mySetFontSize(IDC_BUTTON1, CONTROLER_SIZE);		// 打开

	mySetFontSize(IDC_STATIC1, CONTROLER_SIZE);		// 所有障碍物
	mySetFontSize(IDC_EDIT1, CONTROLER_SIZE);

	mySetFontSize(IDC_STATIC2, CONTROLER_SIZE);		// 移动障碍物
	mySetFontSize(IDC_EDIT2, CONTROLER_SIZE);

	mySetFontSize(IDC_STATIC3, CONTROLER_SIZE);		// 停止障碍物
	mySetFontSize(IDC_EDIT3, CONTROLER_SIZE);

	mySetFontSize(IDC_STATIC4, CONTROLER_SIZE);		// 静止障碍物
	mySetFontSize(IDC_EDIT4, CONTROLER_SIZE);

	mySetFontSize(IDC_STATIC5, CONTROLER_SIZE);		// 车速
	mySetFontSize(IDC_EDIT5, CONTROLER_SIZE);		// 车速

	mySetFontSize(IDC_STATIC6, CONTROLER_SIZE);		// 加速度
	mySetFontSize(IDC_EDIT6, CONTROLER_SIZE);		//

	mySetFontSize(IDC_STATIC7, CONTROLER_SIZE);		// 拐弯角度
	mySetFontSize(IDC_EDIT7, CONTROLER_SIZE);		// 

	// csv文件解析
	mySetFontSize(IDC_BUTTON2, CONTROLER_SIZE);		// 
	mySetFontSize(IDC_MFCEDITBROWSE1, CONTROLER_SIZE);		// 
	
	mySetFontSize(IDC_COMBO2, CONTROLER_SIZE);				// CAN标识ID
	mySetFontSize(IDC_STATIC8, CONTROLER_SIZE);				// CAN标识ID

	// CAN下标添加标识
	for (int i = 0; i < 8; i++) {
		char index_str[4] = { 0 };
		sprintf(index_str, "%d", i);
		m_combox_can_index_handle.InsertString(i, index_str);
	}
	m_combox_can_index_handle.SetCurSel(0);

	mySetFontSize(IDC_COMBO1, CONTROLER_SIZE);				// 是否显示内容listbox
	m_combox_show_listbox_handle.InsertString(0, "不显示");
	m_combox_show_listbox_handle.InsertString(1, "显示");
	m_combox_show_listbox_handle.SetCurSel(0);
	// 时间小时分钟 combox控件】
	mySetFontSize(IDC_COMBO3, CONTROLER_SIZE);				// 时间标识ID
	mySetFontSize(IDC_COMBO4, CONTROLER_SIZE);				// 标识ID
	mySetFontSize(IDC_COMBO5, CONTROLER_SIZE);				// 标识ID
	mySetFontSize(IDC_COMBO6, CONTROLER_SIZE);				// 标识ID	
	mySetFontSize(IDC_COMBO7, CONTROLER_SIZE);				// 标识ID
	mySetFontSize(IDC_COMBO8, CONTROLER_SIZE);				// 标识ID

	mySetFontSize(IDC_STA_ZHI, CONTROLER_SIZE);				// 至
	
	for (int i = 0; i <= 24; i++) {
		char index_str[4] = { 0 };
		sprintf(index_str, "%d", i);
		m_combox_begin_hour_handle.InsertString(i, index_str);
		m_combox_end_hour_handle.InsertString(i, index_str);
	}
	m_combox_begin_hour_handle.SetCurSel(12);
	m_combox_end_hour_handle.SetCurSel(12);
	for (int i = 0; i <= 60; i++) {
		char index_str[4] = { 0 };
		sprintf(index_str, "%d", i);
		m_combox_begin_min_handle.InsertString(i, index_str);
		m_combox_end_min_handle.InsertString(i, index_str);
	}
	m_combox_begin_min_handle.SetCurSel(30);
	m_combox_end_min_handle.SetCurSel(30);

	for (int i = 0; i <= 60; i++) {
		char index_str[4] = { 0 };
		sprintf(index_str, "%d", i );
		m_combox_begin_second_handle.InsertString(i, index_str);
		m_combox_end_second_handle.InsertString(i, index_str);
	}
	m_combox_begin_second_handle.SetCurSel(30);
	m_combox_end_second_handle.SetCurSel(30);

}
// CAutelAS100Dlg 消息处理程序

BOOL CAutelAS100Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	//AllocConsole();								// 开辟控制台
	//SetConsoleTitle(_T("Debug Dialog"));		// 设置控制台窗口标题
	//freopen("CONOUT$", "w", stdout);            // 重定向输出
	//printf("*******************Debug*******************");
	
	SetBackgroundColor(RGB(120, 163, 222));		// 浅色蓝色1


	// 设置文字大小
	Dlg_Front_Init();

	// 固定大小
	::SetWindowLong(m_hWnd, GWL_STYLE, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX);

	// 浏览文件
	m_editBrowse_handle.EnableFileBrowseButton(_T(""), _T("清单逗号文件 (*.csv)|*.csv|All Files (*.*)|*.*|"));

	// 启动数据处理线程
	m_thread_process_is_stop = false;
	AfxBeginThread(ProcessThread, this);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CAutelAS100Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CAutelAS100Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CAutelAS100Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CAutelAS100Dlg::OnBnClickedButton1()
{
	// 若解析中就要提示要先关闭，在执行
	if (m_btn_anlysis_sw) {
		List_Show_Info("请先结束解析，再执行采集。");
		return;
	}
	//printf("00000000000\n");
	if (m_btn_func_sw) {
		//printf("11111111111111111\n");
		m_btn_func_sw = false;
		m_thread_recv_is_stop = true;
		m_thread_process_is_working = false;

		VCI_CloseDevice(m_devtype, m_devind);

		// 关闭车速等信息的显示
		KillTimer(1);
		m_btn_func_sw_handle.SetWindowTextA("开始采集");	


		// 关闭文件
		if (pFile_handle != NULL) {
			fclose(pFile_handle);
			pFile_handle = NULL;
		}
	}
	else {
		//printf("2222222222222222\n");
		// 打开写文件
		Create_File_To_Write_Log_Info();

		// 获取CAN索引
		m_devind = m_combox_can_index_handle.GetCurSel();
		//if(start_CAN(m_devind, BR_500K)==false){
		if(start_CAN()==false){
			List_Show_Info("CAN打开失败.");
			return ;
		}
		// 获取Listbox是否显示内容
		if (m_combox_show_listbox_handle.GetCurSel() == 0) {
			m_listbox_is_show = false;
		}
		else m_listbox_is_show = true;

		m_btn_func_sw = true;
		m_thread_recv_is_stop = false;
		/* 开启接受线程 */
		m_btn_func_sw_handle.SetWindowTextA("结束采集");
		AfxBeginThread(ReceiveThread, this);
		m_thread_process_is_working = true;

		SetTimer(1, 100, NULL);
	}
}


//UINT WINAPI CAutelAS100Dlg::Autel_ASR100_Thread_Process(void* pParam)
UINT CAutelAS100Dlg::ProcessThread(void *param)
{
#define DEBUG_PRINT			0
#define JUDGE_PKG_NUM		9
	/** 得到本类的指针 */
	CAutelAS100Dlg* pthread = reinterpret_cast<CAutelAS100Dlg*>(param);
	struct Judge_Str l_judge_str = { false, 1, 0, 0, 0, 0, 0, 0, 0 };
	// 用于一次性显示数据
	queue<struct SRRObstacle> l_show_queue;
	//queue<struct Show_Info_Str> l_show_queue;
	//struct Show_Info_Str l_shwo_str;
	printf("处理逻辑\n");

	while (!pthread->m_thread_process_is_stop) {

		if (!pthread->m_thread_process_is_working) Sleep(100);
		//Sleep(1);
		// 异常处理，若长度超出9帧就开始处理
		//if (pthread->can_handle->m_canInfo_queue.size() > 9) 
		//	goto START_ANALYSIS;

		if (!l_judge_str.is_full_9_frame_sw) {		// 超过9帧就处理
			if (!pthread->m_canInfo_queue.empty()) {
				if (pthread->m_canInfo_queue.size() < JUDGE_PKG_NUM) continue;
				else {
					//START_ANALYSIS:
					l_judge_str.pop_cnt = 1;
					l_judge_str.array_index_multi = 0;
					l_judge_str.move_obj_num = 0;
					l_judge_str.static_obj_num = 0;
					l_judge_str.stop_obj_num = 0;
					l_judge_str.is_full_9_frame_sw = true;
					l_judge_str.object_num = pthread->m_canInfo_queue.size();
					// 仅处理9的倍数条数据
					l_judge_str.dispose_times = l_judge_str.object_num / JUDGE_PKG_NUM * JUDGE_PKG_NUM;
					printf("queue size:%d,dipose_times:%d\n", l_judge_str.object_num, l_judge_str.dispose_times);
				}
			}
		}
		else {
			// 一次性处理9的倍数包的数据
			if (l_judge_str.pop_cnt > l_judge_str.dispose_times) {
				l_judge_str.is_full_9_frame_sw = false;
				//printf("object_dispose_num:%d\n", l_judge_str.pop_cnt-1);
				// 贴数据
				while (!l_show_queue.empty()) {
#if 1
					struct SRRObstacle show_info = l_show_queue.front();
#else
					struct Show_Info_Str show_info = l_show_queue.front();
#endif
					l_show_queue.pop();
					pthread->In_Listbox_Show_SRR_Info(&show_info);
				}
				continue;
			}
			else {
				l_judge_str.pop_cnt++;
			}

			CAN_Show_Info m_can_info = pthread->m_canInfo_queue.front();
			pthread->m_canInfo_queue.pop();

			// 分去除不完整的包解析数据，和不去除完整包进行解析显示
			switch (m_can_info.CanInfo.ID) {
			case 0x756:
			case 0x753:
			case 0x750: {
				if (m_can_info.CanInfo.ID == 0x756) l_judge_str.array_index = 2;
				else if (m_can_info.CanInfo.ID == 0x753) l_judge_str.array_index = 1;
				else l_judge_str.array_index = 0;

				pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order1 = m_can_info.CanInfo.Data[0];										// 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].object_id = m_can_info.CanInfo.Data[1];										// 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].warning_grade = m_can_info.CanInfo.Data[2];									// 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].tracking_num = m_can_info.CanInfo.Data[3];									// 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].ttc.x = (float)(m_can_info.CanInfo.Data[4] * 0.05f);							// 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].ttc.y = (float)(m_can_info.CanInfo.Data[5] * 0.05f);							// 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].warning_zone = (m_can_info.CanInfo.Data[6] & 0x0F);			// 4bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].motion_state = (m_can_info.CanInfo.Data[6] >> 4);			// 4bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].malfunction = (m_can_info.CanInfo.Data[7] & 0x0F);			// 4bit 

				//if(pthread->m_srr_obstacle_info[l_judge_str.array_index].object_id == 0)
				//	printf("dispose_track_num:%d\n", m_can_info.CanInfo.Data[3]);
				// 计算障碍物的个数
				switch (pthread->m_srr_obstacle_info[l_judge_str.array_index].motion_state) {
				case UNKNOWN_MOTION_STATE: {}break;
				case MOVING_MOTION_STATE: {pthread->m_srr_obstacle_info[l_judge_str.array_index].move_obj_num++; }break;
				case STOPED_MOTION_STATE: {pthread->m_srr_obstacle_info[l_judge_str.array_index].stop_obj_num++; }break;
				case STANDING_MOTION_STATE: {pthread->m_srr_obstacle_info[l_judge_str.array_index].static_obj_num++; }break;
				}

				pthread->m_srr_obstacle_info[l_judge_str.array_index].system_time = 0;// m_can_info.CanInfo.recv_time_ms;

				//l_show_queue.push(pthread->m_srr_obstacle_info[l_judge_str.array_index]); // 测试
			}break;
			case 0x757:
			case 0x754:
			case 0x751: {
				if (m_can_info.CanInfo.ID == 0x757) l_judge_str.array_index = 2;
				else if (m_can_info.CanInfo.ID == 0x754) l_judge_str.array_index = 1;
				else l_judge_str.array_index = 0;

				pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order2 = m_can_info.CanInfo.Data[0];										// 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].dist.x = (float)(m_can_info.CanInfo.Data[2] << 8 | m_can_info.CanInfo.Data[1]) * 0.02f - 655.36f;					// 16bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].dist.y = (float)((m_can_info.CanInfo.Data[4] & 0x0F) << 4 | m_can_info.CanInfo.Data[3]) * 0.02f - 40.96f;		// 12bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].vel.x = (float)(m_can_info.CanInfo.Data[5] << 4 | (m_can_info.CanInfo.Data[4] & 0xF0) >> 4) * 0.1f - 204.8f;	// 12bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].vel.y = (float)((m_can_info.CanInfo.Data[7] & 0x03) << 2 | m_can_info.CanInfo.Data[6]) * 0.1f - 51.2f;			// 10bit

				//if (pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order1 == pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order2 && \
				//	pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order2 == pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order3) {
				//l_show_queue.push(pthread->m_srr_obstacle_info[l_judge_str.array_index]);
				//}
			}break;
			case 0x758:
			case 0x755:
			case 0x752: {

				if (m_can_info.CanInfo.ID == 0x758) l_judge_str.array_index = 2;
				else if (m_can_info.CanInfo.ID == 0x755) l_judge_str.array_index = 1;
				else l_judge_str.array_index = 0;

				pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order3 = m_can_info.CanInfo.Data[0];
				pthread->m_srr_obstacle_info[l_judge_str.array_index].acc.x = (float)(m_can_info.CanInfo.Data[4]) * 0.2f - 25.6f;		// 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].acc.y = (float)(m_can_info.CanInfo.Data[5]) * 0.2f - 25.6f;		// 8bit
				
				if (strlen(m_can_info.TimeStr) > 2) {
					pthread->m_srr_obstacle_info[l_judge_str.array_index].is_valid_time = true;
					memcpy(pthread->m_srr_obstacle_info[l_judge_str.array_index].time_stamp_str, m_can_info.TimeStr, strlen(m_can_info.TimeStr));// 时间戳
				}
				else {
					pthread->m_srr_obstacle_info[l_judge_str.array_index].is_valid_time = false;
					memset(pthread->m_srr_obstacle_info[l_judge_str.array_index].time_stamp_str, 0, sizeof(pthread->m_srr_obstacle_info[l_judge_str.array_index].time_stamp_str));
				}

				if (pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order1 == pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order2 && \
					pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order2 == pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order3) {
#if 1					// 将0x760包中的车速信息一起加到queue包中
					if (pthread->m_srr_obstacle_info[l_judge_str.array_index].is_valid_time) {
						while (!pthread->m_canSpeed_queue.empty()) {
							CAN_Show_Info l_carSpeed_info = pthread->m_canSpeed_queue.front();
							
							// 时间判断条件，到百毫秒级别 16:59:27:4
							if (strncmp(pthread->m_srr_obstacle_info[l_judge_str.array_index].time_stamp_str, l_carSpeed_info.TimeStr, 10) == 0) {
								// 找到了就解析，车速、加速度、角速度信息，并添加到
								float car_speed = 0.0f;
								float car_acceleration = 0.0f;
								float car_angle_speed = 0.0f;
								//pthread->vehicleCan_info.right_turn = frameinfo[i].Data[0] & 0x01;
								//pthread->vehicleCan_info.left_turn = (frameinfo[i].Data[0] >> 1) & 0x01;
								//pthread->vehicleCan_info.reversing = (frameinfo[i].Data[0] >> 3) & 0x03;
								//pthread->vehicleCan_info.brake_status = (frameinfo[i].Data[0] >> 5) & 0x03;

								car_speed = ((float)(l_carSpeed_info.CanInfo.Data[1] | (l_carSpeed_info.CanInfo.Data[2] << 8)) - 0.5f) / 3.6f / 128.0f;

								if (l_carSpeed_info.CanInfo.Data[3] == 0xFF && l_carSpeed_info.CanInfo.Data[4] == 0xFF) car_angle_speed = 0.0f;
								else car_angle_speed = (float)(((l_carSpeed_info.CanInfo.Data[3] + l_carSpeed_info.CanInfo.Data[4] * 255) - 0.5f) / 100.0 - 327.67);

								if (l_carSpeed_info.CanInfo.Data[5] == 0xFF && l_carSpeed_info.CanInfo.Data[6] == 0xFF) car_acceleration = 0.0f;
								else car_acceleration = (float)(((l_carSpeed_info.CanInfo.Data[5] + l_carSpeed_info.CanInfo.Data[6] * 255) - 0.5f) * 16.0 / 32768.0 - 16.0);

								pthread->m_srr_obstacle_info[l_judge_str.array_index].car_speed = car_speed;
								pthread->m_srr_obstacle_info[l_judge_str.array_index].car_acceleration = car_acceleration;
								pthread->m_srr_obstacle_info[l_judge_str.array_index].car_angle_speed = car_angle_speed;

								// 是否启动一个触发开关
								pthread->m_srr_obstacle_info[l_judge_str.array_index].is_valid_carSpeed = true;
								break;
							}

							pthread->m_canSpeed_queue.pop();
						}
					}
#endif
					l_show_queue.push(pthread->m_srr_obstacle_info[l_judge_str.array_index]);
				}
				//l_show_queue.push(pthread->m_srr_obstacle_info[l_judge_str.array_index]); // 测试
				memset(&pthread->m_srr_obstacle_info[l_judge_str.array_index], 0, sizeof(struct SRRObstacle));
			}break;
			}
		}
	}
	printf("处理数据结束\n");
	return 0;
}


void CAutelAS100Dlg::In_Listbox_Show_SRR_Info(struct SRRObstacle *p_info)
{
	if (p_info->tracking_num <= 0) return;		// 跟踪障碍物个数为0
	if (p_info->object_id == 0xFF) return;		// 无效障碍物


	static int write_times = 0;
	char print_buf[250] = { 0 };

	if (p_info->is_valid_time && strlen(p_info->time_stamp_str) > 5) {
		p_info->is_valid_time = false;
		sprintf(print_buf, "%s  ", p_info->time_stamp_str);
	}
	else {
		SYSTEMTIME st;
		GetLocalTime(&st);
		//printf("%4d-%02d-%02d %02d:%02d:%02d\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
		sprintf(print_buf, "%02d-%02d %02d:%02d:%02d.%03d  ", st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
	}
	if (p_info->object_id < 10) sprintf(print_buf + strlen(print_buf), "ID:   %d ", p_info->object_id);
	else if (p_info->object_id < 100) sprintf(print_buf + strlen(print_buf), "ID:  %d ", p_info->object_id);
	else sprintf(print_buf + strlen(print_buf), "ID:%d ", p_info->object_id);

	switch (p_info->warning_grade) {
	case 0:sprintf(print_buf + strlen(print_buf), "预警:无  "); break;
	case 1:sprintf(print_buf + strlen(print_buf), "预警:绿  "); break;
	case 2:sprintf(print_buf + strlen(print_buf), "预警:黄  "); break;
	case 3:sprintf(print_buf + strlen(print_buf), "预警:声  "); break;
	default:sprintf(print_buf + strlen(print_buf), "预警:无  "); break;
	}

	if (p_info->tracking_num < 10) sprintf(print_buf + strlen(print_buf), "标记ID:   %d  ", p_info->tracking_num);
	else if (p_info->tracking_num < 100) sprintf(print_buf + strlen(print_buf), "标记ID: %d  ", p_info->tracking_num);
	else sprintf(print_buf + strlen(print_buf), "标记ID:%d  ", p_info->tracking_num);

	switch (p_info->warning_zone) {
	case UNKNOWN_CAR_ZONE:sprintf(print_buf + strlen(print_buf), "待定"); break;
	case RIGHT_MIDDLE_CAR_ZONE:sprintf(print_buf + strlen(print_buf), "右侧"); break;
	case RIGHT_BACK_CAR_ZONE:sprintf(print_buf + strlen(print_buf), "右后"); break;
	case RIGHT_FRONT_CAR_ZONE:sprintf(print_buf + strlen(print_buf), "右前"); break;
	default:sprintf(print_buf + strlen(print_buf), "待定"); break;
	}

	switch (p_info->motion_state) {
	case UNKNOWN_MOTION_STATE:sprintf(print_buf + strlen(print_buf), "|待定 "); break;
	case MOVING_MOTION_STATE:sprintf(print_buf + strlen(print_buf), "|移动 "); /*p_info->move_obj_num++;*/ break;
	case STOPED_MOTION_STATE:sprintf(print_buf + strlen(print_buf), "|停止 "); /*p_info->stop_obj_num++;*/ break;
	case STANDING_MOTION_STATE:sprintf(print_buf + strlen(print_buf), "|静止 ");/* p_info->static_obj_num++; */break;
	default:sprintf(print_buf + strlen(print_buf), "|待定 "); break;
	}

	sprintf(print_buf + strlen(print_buf), "TTC(X:%6.2f, ", p_info->ttc.x);
	sprintf(print_buf + strlen(print_buf), "Y:%6.2f)  ", p_info->ttc.y);

	sprintf(print_buf + strlen(print_buf), "距离:(X:%6.2f, ", p_info->dist.x);
	sprintf(print_buf + strlen(print_buf), "Y:%6.2f)  ", p_info->dist.y);

	sprintf(print_buf + strlen(print_buf), "速度(X:%6.2f, ", p_info->vel.x);
	sprintf(print_buf + strlen(print_buf), "Y:%6.2f)  ", p_info->vel.y);

	sprintf(print_buf + strlen(print_buf), "加速度(X:%6.2f, ", p_info->acc.x);
	sprintf(print_buf + strlen(print_buf), "Y:%6.2f) ", p_info->acc.y);

	// 添加车速度、加速度、角速度信息
	if (p_info->is_valid_carSpeed) {
		p_info->is_valid_carSpeed = false;
		sprintf(print_buf + strlen(print_buf), "车速:%6.2f, ", p_info->car_speed);
		sprintf(print_buf + strlen(print_buf), "加速度:%6.2f, ", p_info->car_acceleration);
		sprintf(print_buf + strlen(print_buf), "角速度:%6.2f\n", p_info->car_angle_speed);
	}
	else {
		sprintf(print_buf + strlen(print_buf), "\n");
	}
	
	//switch (p_info->malfunction) {
	//case SENSOR_OK:sprintf(print_buf + strlen(print_buf), "正常 "); break;
	//case SENSOR_FAILURE:sprintf(print_buf + strlen(print_buf), "传感器失败 "); break;
	//case SENSOR_PERFORM_DECREASED:sprintf(print_buf + strlen(print_buf), "执行力降低 "); break;
	//case SENSOR_COMMUNICATION_FAILURE:sprintf(print_buf + strlen(print_buf), "通讯失败 "); break;
	//default:sprintf(print_buf + strlen(print_buf), "正常 "); break;
	//}

	//sprintf(print_buf + strlen(print_buf), "用时:%lld\n", p_info->system_time);
	if (m_listbox_is_show) {
		List_Show_Info(print_buf);
	}

	if (pFile_handle != NULL) {
		// 写文件
		fwrite(print_buf, sizeof(char), strlen(print_buf), pFile_handle);
		// 存储10000条，就换文件存储
		if (write_times > 30000) {
			write_times = 0;
			fclose(pFile_handle);
			Create_File_To_Write_Log_Info();
		}
		else write_times++;
	}

	// 显示检测到障碍物的个数
	CString cstr_num("");
	p_info->total_obj_num = p_info->move_obj_num + p_info->stop_obj_num + p_info->static_obj_num;
	cstr_num.Format("%d", p_info->total_obj_num);
	//cstr_num.Format("%d", p_info->tracking_num);
	m_edit_object_num_handle.SetWindowTextA(cstr_num);

	cstr_num.Format("%d", p_info->move_obj_num);
	m_edit_move_object_num_handle.SetWindowTextA(cstr_num);

	cstr_num.Format("%d", p_info->stop_obj_num);
	m_edit_stop_object_num_handle.SetWindowTextA(cstr_num);

	cstr_num.Format("%d", p_info->static_obj_num);
	m_edit_static_object_num_handle.SetWindowTextA(cstr_num);


}

void CAutelAS100Dlg::List_Show_Info(char* data)
{
	CString str(data);
	m_lis_show_info_handle.AddString(str);

	// 自动滚动
	m_lis_show_info_handle.SetCurSel(m_lis_show_info_handle.GetCount() - 1);

}

void CAutelAS100Dlg::Create_File_To_Write_Log_Info()
{
	// 打开文件,写数据
	// 获取当前文件路径
	char m_hexFile_path[MAX_PATH];			// hex配置文件路径
	TCHAR sHexPath[MAX_PATH] = { 0 };
	GetCurrentDirectory(MAX_PATH, sHexPath);
	strcpy(m_hexFile_path, sHexPath);

	SYSTEMTIME st;
	GetLocalTime(&st);

	char l_fileName[100] = { 0 };
	sprintf(l_fileName, "\\SSR_Detection_Result_%04d%02d%02d%02d%02d%02d.txt", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
	strcat(m_hexFile_path, l_fileName);
	//printf("path:%s\n", m_hexFile_path);

	CString m_file_name("");
	m_file_name = m_hexFile_path;
	errno_t err = _wfopen_s(&pFile_handle, m_file_name.AllocSysString(), L"w"); //打开文件strFilePath是文件路径VS2010是UNICODE编码 定义时注意转换或者这样L"xxx"
	if (err == NULL) //判断文件是否打开成功
	{
		//List_Show_Info("打开文件失败。");
		return ;
	}
}

void CAutelAS100Dlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CDialogEx::OnTimer(nIDEvent);

	switch (nIDEvent) {
	case 0: {	// 50ms清空一次listbox数据
		//int count = m_lis_show_info_handle.GetCount();
		//for (int i = count; i >= 0; i--) {
		//	m_lis_show_info_handle.DeleteString(i);
		//}

		// 清空数据显示缓冲区
		//memset(m_srr_obstacle_info, 0, sizeof(struct SRRObstacle) * OBSTACLES_GROUP_NUM);
	}break;
	case 1: {	// 显示车速等信息
		CString show_info_str("");
		show_info_str.Format("%0.2f", vehicleCan_info.car_speed);
		m_edit_vehicle_spd_handle.SetWindowTextA(show_info_str);

		show_info_str.Format("%0.2f", vehicleCan_info.x_accel);
		m_imu_x_acc_handle.SetWindowTextA(show_info_str);

		show_info_str.Format("%0.2f", vehicleCan_info.yaw_rate);
		m_imu_z_yaw_handle.SetWindowTextA(show_info_str);
	}break;
	}
}

#define READ_CSV_LEN		20
typedef struct _Read_Mark_Str {
	char timestamp[READ_CSV_LEN];
	bool ssrInfo_isValid;
	bool carSpeed_isValid;
}Read_Mark_Str;
Read_Mark_Str read_mark_str = { 0 ,false,false };

UINT CAutelAS100Dlg::AnalyCSVFileThread(void *param)
{
	CAutelAS100Dlg *pthread = (CAutelAS100Dlg*)param;

	CAN_Show_Info frameinfo_ssr_csv = { 0 };
	CAN_Show_Info frameinfo_carSpeed_csv = { 0 };
	unsigned char cTemp;
	char buf[READ_CSV_LEN] = {0};//定义缓冲区
	int i = 0;
	memset(buf, 0, READ_CSV_LEN);//缓冲区清零
	int nIndex = 0;
	int j = 0;
	//long seq_ = 0;
	//printf("totalLen:%ld\n", pthread->m_csv_file_length);
	printf("analysis csv file thread\n");

	//主要内容
	while(!pthread->m_thread_csv_is_stop && i < pthread->m_csv_file_length)
	{
		i++;

		pthread->m_csv_cfile_handle.Read(&cTemp, 1);//从文件中读取一个字节
		if (cTemp == '\n')//是否读取到了换行符
		{
			nIndex = 0;
			memset(&frameinfo_ssr_csv, 0, sizeof(VCI_CAN_OBJ));
			memset(&frameinfo_carSpeed_csv, 0, sizeof(VCI_CAN_OBJ));
		}
		if ((cTemp == ',') || (cTemp == '\n'))//是否读取到了逗号或者换行符,即得到了一个完整数据
		{
			switch (nIndex) {
			case 2: {	// 在时间上添加过滤条件
				if (pthread->m_time_is_filter) {	// 时间过滤条件，开启
					if (pthread->m_time_filter_is_trig == false) {	// 时间没有触发前，找开始时间
						if (strncmp(pthread->m_time_begin_filter_str, buf, 8) == 0) {	// 找到开始时间，就认为事件触发了
							pthread->m_time_filter_is_trig = true;
						}
					}
					else {	// 事件触发了，找结束（停止）时间
						if (strncmp(pthread->m_time_end_filter_str, buf, 8) == 0) {	// 找到开始时间，就认为事件结束了
							pthread->m_time_filter_is_trig = false;
							pthread->m_time_is_filter = false;
							pthread->m_thread_csv_is_stop = false;
							pthread->m_csv_file_length = 0;
						}
					}
					memcpy(read_mark_str.timestamp, buf, j);
				}else {
					memcpy(read_mark_str.timestamp, buf, j);
				}
			}break;	// timestamp
			case 3: {	// frameid
				if (pthread->m_time_filter_is_trig) {	// 触发了才会执行
					if (strncmp(buf, "0x750", 5) == 0 || \
						strncmp(buf, "0x751", 5) == 0 || \
						strncmp(buf, "0x752", 5) == 0 || \
						strncmp(buf, "0x753", 5) == 0 || \
						strncmp(buf, "0x754", 5) == 0 || \
						strncmp(buf, "0x755", 5) == 0 || \
						strncmp(buf, "0x756", 5) == 0 || \
						strncmp(buf, "0x757", 5) == 0 || \
						strncmp(buf, "0x758", 5) == 0) {
						//printf("\n%s  ", read_mark_str.timestamp);
						memcpy(frameinfo_ssr_csv.TimeStr, read_mark_str.timestamp, strlen(read_mark_str.timestamp));
						read_mark_str.ssrInfo_isValid = true;
						//printf("%s  ", buf);
						frameinfo_ssr_csv.CanInfo.ID = (buf[2] - 0x30) * 16 * 16 + (buf[3] - 0x30) * 16 + (buf[4] - 0x30);
						//printf("0x%03X  ", frameinfo_ssr_csv.CanInfo.ID);
					}
					else {
						read_mark_str.ssrInfo_isValid = false;
					}
				}

				// 找到0x760数据入队，为了找到相同时间的车速、加速度、转角信息
				if (strncmp(buf, "0x760", 5) == 0){
					memcpy(frameinfo_carSpeed_csv.TimeStr, read_mark_str.timestamp, strlen(read_mark_str.timestamp));
					read_mark_str.carSpeed_isValid = true;
					//printf("%s  ", buf);
					frameinfo_carSpeed_csv.CanInfo.ID = (buf[2] - 0x30) * 16 * 16 + (buf[3] - 0x30) * 16 + (buf[4] - 0x30);
					//printf("0x%03X  ", frameinfo_csv.ID);
				}
				else {
					read_mark_str.carSpeed_isValid = false;
				}
			}break;
			case 4:		// data[0]
			case 5:		// data[1]
			case 6:		// data[2]
			case 7:		// data[3]
			case 8:		// data[4]
			case 9:		// data[5]
			case 10:	// data[6]
			case 11:	// data[7]
			{
				if (read_mark_str.ssrInfo_isValid) {	// 角雷达信息
					UINT8 data = 0;
					if (j == 1) {
						if (buf[0] >= 'a' && buf[0] <= 'f') data = buf[0] - 0x57;
						else data = buf[0] - 0x30;
					}
					else {
						// 0 high; 1 low
						if (buf[0] >= 'a' && buf[0] <= 'f') data = (buf[0] - 0x57) * 16;
						else data = (buf[0] - 0x30) * 16;

						if (buf[1] >= 'a' && buf[1] <= 'f') data += buf[1] - 0x57;
						else data += buf[1] - 0x30;
					}

					frameinfo_ssr_csv.CanInfo.Data[nIndex - 4] = data;
					//printf("%02X  ", data);
					// 封包
					if (nIndex == 11) {
						pthread->m_canInfo_queue.push(frameinfo_ssr_csv);
					}
				}

				if (read_mark_str.carSpeed_isValid) {	// 车速信息
					UINT8 data = 0;
					if (j == 1) {
						if (buf[0] >= 'a' && buf[0] <= 'f') data = buf[0] - 0x57;
						else data = buf[0] - 0x30;
					}
					else {
						// 0 high; 1 low
						if (buf[0] >= 'a' && buf[0] <= 'f') data = (buf[0] - 0x57) * 16;
						else data = (buf[0] - 0x30) * 16;

						if (buf[1] >= 'a' && buf[1] <= 'f') data += buf[1] - 0x57;
						else data += buf[1] - 0x30;
					}

					frameinfo_carSpeed_csv.CanInfo.Data[nIndex - 4] = data;
					//printf("%02X  ", data);
					// 封包
					if (nIndex == 11) {
						pthread->m_canSpeed_queue.push(frameinfo_carSpeed_csv);
					}
				}

			}break;
			}
			nIndex++;

			memset(buf, 0, READ_CSV_LEN);//清空缓冲区

			j = 0;
			continue;
		}
		buf[j++] = cTemp;
	}

	pthread->m_csv_cfile_handle.Close();
	printf("解析csv文件结束.\n");

	return 0;
}
void CAutelAS100Dlg::OnBnClickedButton2()
{
	if (m_btn_func_sw) {
		List_Show_Info("请先停止采集，再执行解析。");
		return;
	}

	if (m_btn_anlysis_sw) {	// 关闭
		m_btn_anlysis_sw = false;
		m_thread_csv_is_stop = true;
		m_thread_process_is_working = false;
		m_listbox_is_show = false;
		m_time_filter_is_trig = false;
		m_btn_cvs_analysis_handle.SetWindowTextA("开始解析");

		// 关闭文件
		if (pFile_handle != NULL) {
			fclose(pFile_handle);
			pFile_handle = NULL;
		}
	}
	else {	//打开

		CString cvs_name("");
		// 获取文件地址和名称
		m_editBrowse_handle.GetWindowTextA(cvs_name);

		if (cvs_name.GetLength() <= 2) {
			List_Show_Info("请先加载csv文件。");
			return;
		}
		// 打开写文件
		Create_File_To_Write_Log_Info();

		if (m_csv_cfile_handle.Open(cvs_name, CFile::modeRead) == FALSE)//CFile::modeRead打开并读取文件
		{
			List_Show_Info("导入的清单文件打不开! 可能已被别的编辑器打开了，请关闭后再试!");
			return;
		}

		// 获取Listbox是否显示内容
		if (m_combox_show_listbox_handle.GetCurSel() == 0) {
			m_listbox_is_show = false;
		}

		else m_listbox_is_show = true;
		// 获取过滤数据的起止时间
		int begin_hour = m_combox_begin_hour_handle.GetCurSel() ;
		int begin_min = m_combox_begin_min_handle.GetCurSel() ;
		int begin_second = m_combox_begin_second_handle.GetCurSel() ;
		int end_hour = m_combox_end_hour_handle.GetCurSel() ;
		int end_min = m_combox_end_min_handle.GetCurSel() ;
		int end_second = m_combox_end_second_handle.GetCurSel() ;
		printf("begin:%02d:%02d:%02d\n", begin_hour, begin_min, begin_second);
		printf("end:%02d:%02d:%02d\n", end_hour, end_min, end_second);
		// 时间相同就不过滤
		if (begin_hour == end_hour && begin_min == end_min && begin_second == end_second) {
			memset(m_time_begin_filter_str, 0, sizeof(m_time_begin_filter_str));
			memset(m_time_end_filter_str, 0, sizeof(m_time_end_filter_str));
			m_time_is_filter = false;	// 不过滤数据
			m_time_filter_is_trig = true;
		}
		else {
			sprintf(m_time_begin_filter_str, "%02d:%02d:%02d", begin_hour, begin_min,begin_second);
			sprintf(m_time_end_filter_str, "%02d:%02d:%02d", end_hour, end_min, end_second);
			m_time_is_filter = true;
		}

		m_csv_file_length = m_csv_cfile_handle.GetLength();//以字节获取文件当前的逻辑长度

		// 创建线程，线程处理数据
		m_thread_csv_is_stop = false;
		AfxBeginThread(AnalyCSVFileThread, this);
		m_thread_process_is_working = true;

		m_btn_anlysis_sw = true;
		m_btn_cvs_analysis_handle.SetWindowTextA("结束解析");
	}

}


